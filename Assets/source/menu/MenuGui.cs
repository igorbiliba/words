﻿using UnityEngine;
using System.Collections;

public class MenuGui : MonoBehaviour {

    public Texture2D exit;
    public Texture2D newGame;
    public Texture2D score3;
    public Texture2D score4;
    public Texture2D score5;
    public Texture2D score6;
    public Texture2D score7;
    public Texture2D score8;


    Rect exitPos;    //810*306
    Rect newGamePos; //810*306
    Rect scorePos;   //850*506

    Texture2D currentScore;

    const float margin = 10f;

    float xNewGame = 0;
    float xExit = 0;
    float yScore = 0;

    bool isAnim = true;
    bool animInner = true;

    // Use this for initialization
    void Start () {
        float w = Screen.width;
        float h = Screen.height;

        this.currentScore = this.getCurrentScore();

        float wButton = w / 5;
        float hButton = wButton / (810 / 306);

        this.newGamePos = new Rect(wButton / 2, h - hButton, wButton, hButton);
        this.exitPos = new Rect(w - wButton - (wButton / 2), h - hButton, wButton, hButton);
        
        float wScore = w / 4;
        float hScore = wScore / (850 / 506);

        this.scorePos = new Rect(w / 2 - (wScore / 2), h - hScore - margin, wScore, hScore);


        //
        this.yScore = this.scorePos.y;
        this.xNewGame = this.newGamePos.x;
        this.xExit = this.exitPos.x;

        //
        this.scorePos.y = (margin + hScore) * -1;
        this.newGamePos.x = (margin + wButton) * -1;
        this.exitPos.x = w + margin;
    }

    const float speedAnim = 500f;

    void Update()
    {
        if(this.isAnim)
        {
            if(this.animInner)
            {
                if (this.scorePos.y < this.yScore) this.scorePos.y += Time.deltaTime*speedAnim;                    
                else this.scorePos.y = this.yScore;

                if (this.newGamePos.x < this.xNewGame) this.newGamePos.x += Time.deltaTime * speedAnim;
                else this.newGamePos.x = this.xNewGame;

                if (this.exitPos.x > this.xExit) this.exitPos.x -= Time.deltaTime * speedAnim;
                else this.exitPos.x = this.xExit;


                if(this.scorePos.y == this.yScore && this.newGamePos.x == this.xNewGame && this.exitPos.x == this.xExit)
                {
                    this.isAnim = false;
                }
            }
            else
            {
                this.scorePos.y -= Time.deltaTime * speedAnim;
                this.newGamePos.x -= Time.deltaTime * speedAnim;
                this.exitPos.x += Time.deltaTime * speedAnim;
            }
        }
    }

    void OnGUI()
    {
        GUI.Button(this.scorePos, this.currentScore, GUIStyle.none);

        if (GUI.Button(this.newGamePos, this.newGame, GUIStyle.none))
        {
            this.isAnim = true;
            this.animInner = false;
            EndLevelEffect ele = gameObject.AddComponent<EndLevelEffect>();
        }
        if(GUI.Button(this.exitPos, this.exit, GUIStyle.none))
        {
            gameObject.AddComponent<EndLevelEffect>();
            Application.Quit();
        }
    }

    Texture2D getCurrentScore()
    {
        int len = SaveScore.getLenMax();

        if (len == 4) return this.score4;
        if (len == 5) return this.score5;
        if (len == 6) return this.score6;
        if (len == 7) return this.score7;
        if (len == 8) return this.score8;

        return this.score3;
    }
}
