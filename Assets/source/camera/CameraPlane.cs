﻿using UnityEngine;
using System.Collections;

public class CameraPlane : MonoBehaviour
{

    public Texture2D planeCamera;
    Rect posPlaneCamera;

    // Use this for initialization
    void Start()
    {
        float w = Screen.width;
        float h = Screen.height;

        this.posPlaneCamera = new Rect(0, 0, w, h);
    }


    void OnGUI()
    {
        GUI.DrawTexture(this.posPlaneCamera, this.planeCamera);
    }

}