﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {
    const float speed = 60f;

    const float offsetY = 3f;

    public GameObject player;
    Vector3 pos;

    public GameObject border_left;
    public GameObject border_right;

    float minLeft = 0;
    float maxLeft = 0;

    const float margin = 5f;

    // Use this for initialization
    void Start () {
        pos = transform.position;

        this.maxLeft = this.border_right.transform.position.x - margin;
        this.minLeft = this.border_left.transform.position.x + margin;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        pos.x += (this.player.transform.position.x - pos.x) / speed;
        pos.y += ((this.player.transform.position.y + offsetY) - pos.y) / speed;

        if (pos.x > this.maxLeft)
        {
            pos.x = this.maxLeft;
        }
        else if (pos.x < this.minLeft)
        {
            pos.x = this.minLeft;
        }

        transform.position = pos;
	}
}
