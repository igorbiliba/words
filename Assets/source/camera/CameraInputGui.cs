﻿using UnityEngine;
using System.Collections;

public class CameraInputGui : MonoBehaviour {
    float horizontal = 0;
    float vertical = 0;

    public bool hideButton = false;

    public GameObject trapObj;
    Trap trap;
    
    float minX = -1000000;
    float maxX = 1000000;

    public Texture2D buttonLeft;
    public Texture2D buttonUp;
    public Texture2D buttonRight;
    public Texture2D buttonForce;
    public Texture2D buttonForce2;

    Rect posButtonLeft;
    Rect posButtonUp;
    Rect posButtonRight;
    Rect posButtonForce;
    Rect posButtonForce2;

    float _posButtonLeft;
    float _posButtonUp;
    float _posButtonRight;
    float _posButtonForce;
    float _posButtonForce2;
    
    public bool startAnimButtons = true;
    float time = 0;

    float playersButtonPosMax = 20f;
    float playersButtonPosCurrent = 0;

    Vector3 pos;

    public GameObject AnimHandL;
    public GameObject AnimHandR;

    public Animator handLAnimator;
    public Animator handRAnimator;

    void InitGuiButtons()
    {

        float w = Screen.width;
        float h = Screen.height;

        float centerW = w / 2;

        float size = 130f;

        float wSize = 2.58f;
        float hMarg = 135f;

        this.posButtonLeft = new Rect(-400f, h - hMarg, size * wSize, size);
        this.posButtonRight = new Rect(-400f + 183f + 10f, h - hMarg, size * wSize, size);
        this.posButtonUp = new Rect(w - 204f + 300f, h - hMarg, size * wSize, size);
        this.posButtonForce = new Rect(w - 204f + 300f, h - hMarg - 15 - hMarg, size * wSize, size);
        this.posButtonForce2 = new Rect(w - 204f + 300f, h - hMarg - 15 - hMarg, size * wSize, size);

        this._posButtonLeft = 5f;
        this._posButtonRight = size * wSize;
        this._posButtonUp = w - (size * wSize) + 70f;
        this._posButtonForce = w - (size * wSize) + 50f;
        this._posButtonForce2 = w - (size * wSize) + 200f;
    }
    
    void OnGUI()
    {
        GUI.RepeatButton(this.posButtonLeft, this.buttonLeft, GUIStyle.none);
        GUI.RepeatButton(this.posButtonRight, this.buttonRight, GUIStyle.none);
        GUI.RepeatButton(this.posButtonUp, this.buttonUp, GUIStyle.none);
        GUI.RepeatButton(this.posButtonForce, this.buttonForce, GUIStyle.none);
        GUI.RepeatButton(this.posButtonForce2, this.buttonForce2, GUIStyle.none);
    }

    
    public bool isTouchContaints(Rect area)
    {
        foreach (Touch touch in Input.touches)
        {
            this.pos = touch.position;
            this.pos.y = Screen.height - this.pos.y;
            if (area.Contains(this.pos))
            {
                return true;
            }
        }

        return false;
    }

    public bool buttonLeftTouch = false;
    public bool buttonUpTouch = false;
    public bool buttonRightTouch = false;
    public bool buttonForceTouch = false;
    public bool buttonForce2Touch = false;

    float timeBeforeToutch = 0;
    const float marginTimeBeforeToutch = 0.5f;

    void GuiUpdate()
    {

        //*********
        this.buttonLeftTouch = this.isTouchContaints(this.posButtonLeft);
        this.buttonUpTouch = this.isTouchContaints(this.posButtonUp);
        this.buttonRightTouch = this.isTouchContaints(this.posButtonRight);

        if(timeBeforeToutch < Time.time) {
            this.buttonForce2Touch = this.isTouchContaints(this.posButtonForce2);

            if (this.buttonForce2Touch) this.buttonForceTouch = false;
            else this.buttonForceTouch = this.isTouchContaints(this.posButtonForce);

            if(this.buttonForceTouch || this.buttonForce2Touch)
            {
                timeBeforeToutch = Time.time + marginTimeBeforeToutch;
            }
        }
        else
        {
            this.buttonForce2Touch = false;
            this.buttonForceTouch = false;            
        }
        //*********

        //*********
        this.horizontal = Input.GetAxis("Horizontal");
        this.vertical = Input.GetAxis("Vertical");

        this.buttonUpTouch = (this.vertical != 0);
        this.buttonLeftTouch = (this.horizontal < 0);
        this.buttonRightTouch = (this.horizontal > 0);

        if (timeBeforeToutch < Time.time)
        {
            this.buttonForce2Touch = Input.GetKeyDown(KeyCode.R);
            this.buttonForceTouch = Input.GetKeyDown(KeyCode.E);

            if (this.buttonForceTouch || this.buttonForce2Touch)
            {
                timeBeforeToutch = Time.time + marginTimeBeforeToutch;
            }
        }
        else
        {
            this.buttonForceTouch = false;
            this.buttonForce2Touch = false;
        }
        //*********

        if(this.buttonLeftTouch || this.buttonRightTouch)
        {
            this.handLAnimator.enabled = true;
            this.handRAnimator.enabled = true;
        }
        else
        {
            this.handLAnimator.enabled = false;
            this.handRAnimator.enabled = false;
        }

        time = Time.deltaTime * 300;

        if (startAnimButtons)
        {

            if (posButtonLeft.x < _posButtonLeft)
            {
                posButtonLeft.x += time;
            }
            else
            {
                posButtonLeft.x = _posButtonLeft;
            }

            if (posButtonRight.x < _posButtonRight)
            {
                posButtonRight.x += time;
            }
            else
            {
                posButtonRight.x = _posButtonRight;
            }

            if (posButtonUp.x > _posButtonUp)
            {
                posButtonUp.x -= time;
            }
            else
            {
                posButtonUp.x = _posButtonUp;
            }

            if (posButtonForce.x > _posButtonForce)
            {
                posButtonForce.x -= time;
            }
            else
            {
                posButtonForce.x = _posButtonForce;
            }

            if (posButtonForce2.x > _posButtonForce2)
            {
                posButtonForce2.x -= time;
            }
            else
            {
                posButtonForce2.x = _posButtonForce2;
            }

            if (this.playersButtonPosCurrent < this.playersButtonPosMax)
            {
                this.playersButtonPosCurrent += time / 2;
            }
            else
            {
                this.playersButtonPosCurrent = this.playersButtonPosMax;
            }
            
            if (posButtonLeft.x == _posButtonLeft &&
               posButtonRight.x == _posButtonRight &&
               posButtonUp.x == _posButtonUp &&
               posButtonForce.x == _posButtonForce &&
               posButtonForce2.x == _posButtonForce2 &&
               this.playersButtonPosCurrent == this.playersButtonPosMax)
            {

                startAnimButtons = false;
            }

        }
        else if (hideButton)
        {
            posButtonLeft.x -= time;
            posButtonRight.x -= time;

            posButtonUp.x += time;
            posButtonForce.x += time;
            posButtonForce2.x += time;

            if (time > 100)
            {
                hideButton = false;
            }
        }
    }


    void Start()
    {
        this.trap = this.trapObj.GetComponent<Trap>();
        this.InitGuiButtons();

        this.handLAnimator = this.AnimHandL.GetComponent<Animator>();
        this.handRAnimator = this.AnimHandR.GetComponent<Animator>();
    }

	void Update () {        
        this.GuiUpdate();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.AddComponent<EndLevelEffect>().loadLevelName = "menu";
        }
    }
}
