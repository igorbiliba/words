﻿using UnityEngine;
using System.Collections;

public class EndLevelEffect : MonoBehaviour {
    float r;
    float g;
    float b;
    float dist;

    Camera c;

    Color col;

    public const float camDistDefault = 60f;
    public const float standardDistDefault = 179f;

    public string loadLevelName = "layout";

    // Use this for initialization
    void Start()
    {
        c = GetComponent<Camera>();

        r = 0.17f;
        g = 0.17f;
        b = 0.17f;
        dist = standardDistDefault;
        
        c.fieldOfView = camDistDefault;
        this.col = c.backgroundColor;

        CameraInputGui cig = GetComponent<CameraInputGui>();
        if(cig != null)
        {
            cig.hideButton = true;
            cig.startAnimButtons = true;
        }
    }

    bool delDist = true;

    float time;
    void Update()
    {
        if (delDist)
        {
            time = Time.deltaTime;
            c.fieldOfView += time * 65;
            if (c.fieldOfView >= dist)
            {
                c.fieldOfView = dist;
                delDist = false;
            }
        }
        else
        {
            if (col.r > r)
            {
                col.r -= time;
            }
            else
            {
                col.r = r;
            }

            if (col.g > g)
            {
                col.g -= time;
            }
            else
            {
                col.g = g;
            }
            if (col.b > b)
            {
                col.b -= time;
            }
            else
            {
                col.b = b;
            }


            c.backgroundColor = col;


            if (col.r == r && col.g == g && col.b == b)
            {
                Application.LoadLevel(this.loadLevelName);
                this.enabled = false;
            }
        }
    }
}
