﻿using UnityEngine;
using System.Collections;

public class StartLevelEffect : MonoBehaviour {
    float r;
    float g;
    float b;
    float dist;

    Camera c;

    Color col;

    public const float camDistDefault = 179f;
    public const float standardDistDefault = 60f;

    // Use this for initialization
    void Start()
    {
        c = GetComponent<Camera>();

        r = 1;
        g = 1;
        b = 1;
        dist = standardDistDefault;
        
        c.fieldOfView = camDistDefault;
        
    }

    bool delDist = true;

    float time;
    void Update()
    {
        if (delDist)
        {
            time = Time.deltaTime;
            c.fieldOfView -= time * 65;
            if (c.fieldOfView <= dist)
            {
                c.fieldOfView = dist;
                delDist = false;
            }
        }
        else
        {
            if (col.r < r)
            {
                col.r += time;
            }
            else
            {
                col.r = r;
            }

            if (col.g < g)
            {
                col.g += time;
            }
            else
            {
                col.g = g;
            }
            if (col.b < b)
            {
                col.b += time;
            }
            else
            {
                col.b = b;
            }


            c.backgroundColor = col;


            if (col.r == r && col.g == g && col.b == b)
            {
                this.enabled = false;
            }
        }
    }
}
