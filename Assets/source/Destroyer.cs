﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {
    public float time = 3f;

	void Update () {
        if (this.time > 0)
        {
            this.time -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
	}
}
