﻿using UnityEngine;
using System.Collections;

public class ItemChar : MonoBehaviour {

    const float animSpeedMin = 10f;
    const float animSpeedMax = 11f;

    float animSpeed = 0f;
    const float animTime = 1f;
    
    int x = 0;
    int y = 0;

    float z = 0;
    float size = 0;

    float startX = 0;
    float startY = 0;

    float freeSpaceX = 0;
    float freeSpaceY = 0;

    float posX = 0;
    float posY = 0;

    Vector3 startPos;
    Vector3 nextPos;

    bool isMove = false;
    float time = 0;

    const float animHaniyehMin = 0.2f;
    public const float animHaniyehMax = 0.4f;
    
    bool isHaniyehExpension = true;
    bool isHaniyeh = false;
    float timeHaniyeh = 0;

    void Start()
    {
        this.animSpeed = Random.Range(animSpeedMin, animSpeedMax);
        this.delayTime = Random.Range(delayTimeMin, delayTimeMax);
    }
    
    public void goHaniyeh()
    {
        this.isHaniyeh = true;
        this.haniyehInit();
    }

    public bool isDelay = true;
    float delayTime = 0f;
    const float delayTimeMin = 2f;
    const float delayTimeMax = 3f;

    public void kill()
    {
        Destroy(transform.gameObject);
    }

    void FixedUpdate () {
        if(this.isDelay)
        {
            if(this.delayTime > 0)
            {
                this.delayTime -= Time.deltaTime;
            }
            else
            {
                this.isDelay = false;
            }
        }
        else if(this.isHaniyeh)
        {
            if (this.timeHaniyeh > 0)
            {
                this.timeHaniyeh -= Time.deltaTime;

                if(this.isHaniyehExpension)
                {
                    transform.localPosition += ((this.nextPos - transform.localPosition) / this.animSpeed);
                }
                else
                {
                    transform.localPosition += ((this.startPos - transform.localPosition) / this.animSpeed);
                }
            }
            else
            {
                this.isHaniyeh = false;

                //без обратного (если закоментить)
                //if(!this.isHaniyehExpension) this.haniyehInit();
            }
        }
        else if (this.isMove)
        {
            if(this.time > 0)
            {
                this.time -= Time.deltaTime;
                transform.localPosition += ((this.nextPos - transform.localPosition) / this.animSpeed);
            }
            else
            {   
                this.isMove = false;
                this.time = 0;
            }
        }
    }

    void haniyehInit()
    {
        this.isHaniyehExpension = !this.isHaniyehExpension;
        this.timeHaniyeh = animTime;
        this.isHaniyeh = true;
    }

    public void setFreeSpace(float _x, float _y)
    {
        this.freeSpaceX = _x;
        this.freeSpaceY = _y;
    }

    public void setAnimSpeed(float v)
    {
        this.animSpeed = v;
    }

    public void setPos(int _x, int _y)
    {
        this.x = _x;
        this.y = _y;

        this.size = transform.parent.localScale.y / CharMatrix.sizeMatrix;

        this.startX = transform.parent.localScale.x / 2 - this.size/2;
        this.startY = transform.parent.localScale.y / 2 - this.size / 2;

        // - count len
        this.startX -= (this.freeSpaceX * this.size);
        this.startY += (this.freeSpaceY * this.size);

        this.transform.localScale = new Vector3(size, size, size);
        this.z = (size * -1) - (transform.parent.localScale.z / 2);
        this.transform.localPosition = new Vector3(0, 0, z);
    }

    public void goMove()
    {
        this.isMove = true;
        this.time = animTime;

        this.posX = (this.x * this.size) - this.startX;
        this.posY = (this.y * this.size) - this.startY;

        this.nextPos = new Vector3(posX, posY, this.z);
        this.startPos = new Vector3(0, 0, this.z);
    }
}
