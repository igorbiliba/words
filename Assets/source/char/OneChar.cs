﻿using UnityEngine;
using System.Collections;

public class OneChar : MonoBehaviour {

    const float forceWin = 600f;

    public const string tagName = "cube";

    public GameObject itemChar;

    public string ch;

    int[,] table;
    ArrayList itemCharList = new ArrayList();

    int countX = 0;
    int countY = 0;

    float freeSpaceX = 0;
    float freeSpaceY = 0;

    float animSpeed = 30f;

    GameObject parentRotate;

    bool isDelay = true;

    public Material defaultMatherial;
    public Material selectedMatherial;

    void createParentRotate()
    {
        this.parentRotate = new GameObject();

        this.parentRotate.transform.parent = gameObject.transform;
        this.parentRotate.transform.localPosition = new Vector3(0,0,0);
    }
    
    public void win()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.None;
        
        rb.AddForce(Vector3.forward * forceWin);

        gameObject.AddComponent<Destroyer>();
    }

    void Start()
    {
        this.createParentRotate();
        this.setChar(this.ch);
    }

    bool isUpdateChar = false;
    float timeUpdateChar = 0;
    public void goUpdateChar(string _ch)
    {
        if (this.ch == _ch) return;

        this.ch = _ch;
        this.isUpdateChar = true;
        this.timeUpdateChar = ItemChar.animHaniyehMax;

        foreach (ItemChar i in this.itemCharList)
        {
            i.goHaniyeh();
        }
    }

    public void goHaniye()
    {
        foreach (ItemChar i in this.itemCharList)
        {
            i.goHaniyeh();
        }
    }

    protected void setChar(string _ch)
    {
        this.countX = 0;
        this.countY = 0;

        CharMatrix cm = new CharMatrix(_ch);
        this.table = cm.getMatrix();

        for (int i = 0; i < CharMatrix.sizeMatrix; i++)
        {
            for (int j = 0; j < CharMatrix.sizeMatrix; j++)
            {
                if (table[j, i] == 1)
                {
                    if (i > this.countX) this.countX = i;
                    if (j > this.countY) this.countY = j;
                }
            }
        }

        this.countX++;
        this.countY++;

        this.table = CharMatrix.RotateMatrix(this.table, 1);

        this.freeSpaceX = (CharMatrix.sizeMatrix - this.countX) / 2;
        this.freeSpaceY = (CharMatrix.sizeMatrix - this.countY) / 2;

        this.instantItems();
    }

    void Update()
    {
        if(this.isUpdateChar)
        {
            if(this.timeUpdateChar > 0)
            {
                this.timeUpdateChar -= Time.deltaTime;
            }
            else
            {
                this.isUpdateChar = false;
                this.killItems();
                this.setChar(this.ch);
            }
        }
    }

    public void killItems()
    {
        foreach(ItemChar ic in this.itemCharList)
        {
            ic.kill();
        }

        this.itemCharList.Clear();
    }

    GameObject go;
    ItemChar ic;
    protected void instantItems()
    {
        for (int i = 0; i < CharMatrix.sizeMatrix; i++)
        {
            for (int j = 0; j < CharMatrix.sizeMatrix; j++)
            {
                if(table[i,j] == 1)
                {
                    //instant
                    this.go = Instantiate(this.itemChar) as GameObject;
                    go.transform.parent = this.parentRotate.transform;
                    go.transform.localPosition = new Vector3(0, 0, 0);

                    //get script
                    this.ic = this.go.GetComponent<ItemChar>();
                    this.ic.isDelay = this.isDelay;
                    itemCharList.Add(this.ic);

                    //set len x, y
                    this.ic.setFreeSpace(this.freeSpaceX, this.freeSpaceY);

                    //setPositionItems
                    this.ic.setPos(i, j);

                    //set anim speed
                    this.ic.setAnimSpeed(this.animSpeed);

                    //goMoveItems
                    this.ic.goMove();
                }
            }
        }

        this.isDelay = false;
    }
}
