﻿using UnityEngine;
using System.Collections;

public class CharSelected : MonoBehaviour {
    MeshRenderer mr;

    void Start () {
        this.mr = GetComponent<MeshRenderer>();
        this.mr.material = gameObject.GetComponent<OneChar>().selectedMatherial;        
	}
	
	public void Rm()
    {
        this.mr = GetComponent<MeshRenderer>();
        this.mr.material = gameObject.GetComponent<OneChar>().defaultMatherial;

        Destroy(this);
    }
}
