﻿using System;
using System.Collections;

public class Algorithms
{
    public ArrayList list = new ArrayList();

    const int maxCountList = 512;

    private int elementLevel = -1;
    private int numberOfElements;
    private int[] permutationValue = new int[0];

    private char[] inputSet;
    public char[] InputSet
    {
        get { return inputSet; }
        set { inputSet = value; }
    }

    private int permutationCount = 0;
    public int PermutationCount
    {
        get { return permutationCount; }
        set { permutationCount = value; }
    }

    public char[] MakeCharArray(string InputString)
    {
        char[] charString = InputString.ToCharArray();
        Array.Resize(ref permutationValue, charString.Length);
        numberOfElements = charString.Length;
        return charString;
    }

    /// <summary>
    /// Recursive Algorithm Source:
    /// A. Bogomolny, Counting And Listing All Permutations from Interactive Mathematics Miscellany and Puzzles
    /// http://www.cut-the-knot.org/do_you_know/AllPerm.shtml, Accessed 11 June 2009
    /// </summary>
    /// <param name="k"></param>
    public void Recursion(int k)
    {
        if (this.list.Count > maxCountList) return;

        elementLevel++;
        permutationValue.SetValue(elementLevel, k);

        if (elementLevel == numberOfElements)
        {
            OutputPermutation(permutationValue);
        }
        else
        {
            for (int i = 0; i < numberOfElements; i++)
            {
                if (permutationValue[i] == 0)
                {
                    Recursion(i);
                }
            }
        }
        elementLevel--;
        permutationValue.SetValue(0, k);
    }

    /// <summary>
    /// Code Source (AddItem()):
    /// A. Bogomolny, Counting And Listing All Permutations from Interactive Mathematics Miscellany and Puzzles
    /// http://www.cut-the-knot.org/do_you_know/AllPerm.shtml, Accessed 11 June 2009
    /// </summary>
    /// <param name="value"></param>
    private void OutputPermutation(int[] value)
    {
        string s = "";
        foreach (int i in value)
        {
            s += inputSet.GetValue(i - 1).ToString();
        }
        
        PermutationCount++;

        this.list.Add(s);
    }
}