﻿using System;
using System.Collections;
using UnityEngine;

public class LineChars {
    public struct ItemCharInLine
    {
        public float x;
        public string ch;
        public Transform obj;        
    }

    ArrayList list = new ArrayList();

    public void add(ItemCharInLine item)
    {
        this.list.Add(item);
    }

    class sortList : IComparer
    {
        public int Compare(object x, object y)
        {
            ItemCharInLine itemX = (ItemCharInLine)x;
            ItemCharInLine itemY = (ItemCharInLine)y;

            if (itemX.x > itemY.x) return 1;
            if (itemX.x < itemY.x) return -1;
            
            return 0;
        }
    }

    IComparer compare;
    public LineChars()
    {
        this.compare = new sortList();
    }

    ArrayList getSort()
    {
        this.list.Sort(this.compare);

        return this.list;
    }

    public string getStr()
    {
        string s = "";

        foreach(ItemCharInLine i in this.getSort())
        {
            s += i.ch;
        }

        return s;
    }

    public void Cls()
    {
        this.list.Clear();
    }
}
