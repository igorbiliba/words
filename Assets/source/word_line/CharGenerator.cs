﻿using UnityEngine;
using System.Collections;

public class CharGenerator : MonoBehaviour {
    CurrentWord currentWord;
    float x = 0;

    public GameObject oneChar;

    const float margin = 2.5f;

    void Start () {
        GetComponent<MeshRenderer>().enabled = false;
    }

    Vector3 pos;
    GameObject go;
    OneChar oc;
    string ch = "";

    bool isGenerate = false;
    float timeGenerate = 0f;
    const float timeGenerateStandard = 0.3f;
    int currentCenerate = 0;
    ArrayList listGenerate;
    
    void Update()
    {
        if (this.isGenerate)
        {
            this.timeGenerate -= Time.deltaTime;

            if (this.timeGenerate <= 0)
            {
                if (this.currentCenerate >= this.listGenerate.Count)
                {
                    this.isGenerate = false;
                    return;
                }

                this.ch = this.listGenerate[this.currentCenerate].ToString();


                //
                this.pos = transform.position;
                pos.x += this.x;
                this.go = Instantiate(this.oneChar, this.pos, Quaternion.identity) as GameObject;
                this.oc = this.go.GetComponent<OneChar>();
                oc.ch = this.ch;

                //передвигаем каретку
                this.x += this.oneChar.transform.localScale.x * margin;
                //
                
                ++this.currentCenerate;
                this.timeGenerate = timeGenerateStandard;
            }
        }
    }

    public void generate(CurrentWord cw)
    {
        this.isGenerate = true;
        this.timeGenerate = timeGenerateStandard;
        this.currentCenerate = 0;

        this.currentWord = cw;
        this.x = 0;

        this.listGenerate = this.currentWord.getRandomizeListChars();
    }
}
