﻿using UnityEngine;
using System.Collections;

public class WordLine : MonoBehaviour {

    public GameObject gameCycle;
    GameCycle cycle;

    public GameObject mabyThe;
    Word mt = null;

    ArrayList list = new ArrayList();

    CurrentWord cw;

    public GameObject coutCharsObj;
    Word countChars;

    public const string defaultMabyVal = "...?";

    void Start()
    {
        this.cycle = this.gameCycle.GetComponent<GameCycle>();
        this.mt = this.mabyThe.GetComponent<Word>();
        this.countChars = this.coutCharsObj.GetComponent<Word>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == OneChar.tagName)
        {
            this.list.Add(other.transform);

            this.check();
        }
    }

    public void setCurrentWord(CurrentWord cw)
    {
        this.cw = cw;
        if (this.mt != null)
        {
            this.mt.s = cw.getDefaultStr();
            this.mt.init();
        }

        if(this.countChars != null)
        {
            this.countChars.s = "(слово из " + cw.getLenStr().ToString() + " букв)";
            this.countChars.init();
        }
    }

    LineChars lc = new LineChars();
    void check()
    {
        this.lc.Cls();

        foreach(Transform oneChar in this.list)
        {
            if (oneChar != null)
            {
                LineChars.ItemCharInLine itemChar = new LineChars.ItemCharInLine();
                itemChar.x = oneChar.position.x;
                itemChar.ch = oneChar.GetComponent<OneChar>().ch;
                itemChar.obj = oneChar;

                this.lc.add(itemChar);
            }
        }

        string w = this.lc.getStr();

        if (this.cw.issetWord(w))
        {
            this.complete();
        }
        else
        {
            this.mtCheck(w);
        }
    }

    void mtCheck(string w)
    {
        string part = this.cw.getPartIsset(w);
        this.mt.s = part;
        this.mt.init();
    }

    void complete()
    {
        this.cycle.complete();
        
        foreach (Transform oneCh in this.list)
        {
            if(oneCh != null)
            {
                OneChar oc = oneCh.GetComponent<OneChar>();
                oc.win();
            }
        }
    }
    
    void OnTriggerExit(Collider other)
    {
        if (other.tag == OneChar.tagName)
        {
            if (other.tag == OneChar.tagName)
            {
                this.list.Remove(other.transform);

                this.check();
            }
        }
    }
}
