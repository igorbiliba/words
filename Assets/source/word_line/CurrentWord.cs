﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentWord {   
    /**
    слова, которые можно составить из этих букв
    */
    ArrayList currentWordsList = new ArrayList();

    const int maxCombinations = 10;
    
    public void clearWordsList()
    {
        this.currentWordsList.Clear();
    }

    public void addWord(string word)
    {
        this.currentWordsList.Add(word);
    }

    public ArrayList getChars()
    {
        ArrayList list = new ArrayList();

        string word = this.getFirstWord();

        for (int i = 0; i < word.Length; i++)
        {
            string ch = word[i].ToString();
            list.Add(ch);
        }

        return list;
    }
    
    protected string getFirstWord()
    {
        if(this.currentWordsList.Count > 0)
        {
            var item = this.currentWordsList[0];
            return item.ToString();
        }

        return null;
    }
    
    /**
    Подходит ли слово?
    */
    public bool issetWord(string word)
    {
        foreach(string item in this.currentWordsList)
        {
            if (item == word) return true;
        }

        return false;
    }
    
    struct partWordStruct
    {
        public string w;
        public int index;

        public string getVal(string wordExample)
        {
            string ret = "";

            for (int i = 0; i < this.index; i++) {
                ret += ".";
            }
            
            ret += this.w;

            int len = wordExample.Length - ret.Length;

            for (int i = 0; i < len; i++)
            {
                ret += ".";
            }

            return ret + "?";
        }
    }

    public string getPartIsset(string word)
    {
        ArrayList list = new ArrayList();

        for(int i = 0; i < word.Length; i++)
        {
            string part = "";
            for(int j = i+1; j <= word.Length; j++)
            {
                int len = j - i;

                if(len > 1)
                {
                    string w = word.Substring(i, len);

                    int index = this.issetPart(w);
                    if (index > -1)
                    {
                        partWordStruct pws = new partWordStruct();
                        pws.w = w;
                        pws.index = index;
                        list.Add(pws);
                    }
                }
            }
        }

        if(list.Count > 0)
        {
            partWordStruct maxWord = new partWordStruct();
            foreach(partWordStruct pws in list)
            {
                if(maxWord.w == null)
                {
                    maxWord = pws;
                }
                else if(pws.w.Length > maxWord.w.Length)
                {
                    maxWord = pws;
                }
            }

            if (maxWord.w != null) return maxWord.getVal(this.getFirstWord());
        }

        return this.getDefaultStr();
    }

    int issetPart(string part)
    {
        foreach(string w in this.currentWordsList)
        {
            int index = w.IndexOf(part);
            if (index > -1)
            {
                return index;
            }
        }

        return -1;
    }

    public int getLenStr()
    {
        return this.getFirstWord().Length;
    }

    public string getDefaultStr()
    {
        string s = "";

        string example = this.getFirstWord();
        for(int i = 0; i < example.Length; i++)
        {
            s += ".";
        }

        return s + "?";
    }

    /*
        отдать комбинацию без совпадений
        попытаться :(((
    */
    ArrayList all;
    public ArrayList getRandomizeListChars()
    {
        ArrayList list = new ArrayList();

        string firstFord = this.getFirstWord();

        this.all = getAllCombinations(firstFord);
        
        int i = 0;
        foreach(string s in all)
        {
            if (!this.issetWord(s))
            {
                list.Add(s);
                if (i++ > maxCombinations) continue;
            }
        }
        
        if(list.Count > 0)
        {
            System.Random r = new System.Random();
            int iRand = r.Next(0, list.Count - 1);
            string retS = list[iRand].ToString();            
            ArrayList retList = new ArrayList();
            foreach(char ch in retS.ToCharArray())
            {
                retList.Add(ch.ToString());
            }

            return retList;
        }

        return null;
    }
    
    public static ArrayList getAllCombinations(string str)
    {
        Algorithms algorithms = new Algorithms();
        algorithms.InputSet = algorithms.MakeCharArray(str);
        algorithms.Recursion(0);

        return algorithms.list;
    }

    public static string getWordByChars(ArrayList chrs)
    {
        string s = "";

        foreach(string i in chrs)
        {
            s += i;
        }

        return s;
    }

    /**
        если текущий набор слов содержит комбинацию, которая не совпадет каким-нибудь словом
    */
    public bool isOk()
    {
        string fw = this.getFirstWord();
        
        return !(this.currentWordsList.Count == this.Factorial(fw.Length));
    }

    int Factorial(int i)
    {
        if (i <= 1)
            return 1;
        return i * Factorial(i - 1);
    }

    /*
        ищем слова, которые имеют теже буквы
    */
    public void FindSiblings(ref List<String> listWords)
    {
        string currentWrod = this.getFirstWord();
        int matches = 0;

        for (int item = 0; item < listWords.Count; item++)
        {
            matches = 0;

            for(int i = 0; i < currentWrod.Length; i++)
            {
                int indexFind = listWords[item].IndexOf(currentWrod[i]);

                if (indexFind > -1)
                {
                    matches++;
                }
                else continue;
            }

            if(matches == currentWrod.Length)
            {
                this.addWord(listWords[item]);
                listWords.RemoveAt(item);
            }
        }
    }
}
