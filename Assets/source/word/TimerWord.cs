﻿using UnityEngine;
using System.Collections;

public class TimerWord : Word {
    public bool isTimer = false;
    float i = 0;

    OneChar[] items;

    public GameObject gameCycle;
    GameCycle cycle;

    const float maxTimeVisible = (60 * 60) - 1;

    override public void init()
    {
        base.init();

        this.items = GetComponentsInChildren<OneChar>();

        this.cycle = this.gameCycle.GetComponent<GameCycle>();
    }

    void FixedUpdate()
    {
        if(this.isTimer)
        {
            if(this.i > 0)
            {
                this.i -= Time.deltaTime;

                string s = ((int)(this.i / 60)).ToString();
                if (s.Length == 1) s = "0" + s;

                s += ":";

                string si = ((int)(this.i % 60)).ToString();
                if (si.Length == 1) si = "0" + si;

                s += si;

                this.UpdateTimer(s);
            }
            else
            {
                this.isTimer = false;
                this.timerEnd();
            }
        }
    }

    protected void UpdateTimer(string s)
    {
        for(int itemS = 0; itemS < s.Length; itemS++)
        {
            string ss = s[itemS].ToString();
            this.items[itemS].goUpdateChar(ss);
        }
    }

    public void addI(float v)
    {
        this.isTimer = true;
        this.i += v;

        if(this.i > maxTimeVisible)
        {
            this.i = maxTimeVisible;
        }
    }

    protected void timerEnd()
    {
        this.cycle.timerOut();
    }
}
