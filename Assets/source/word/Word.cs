﻿using UnityEngine;
using System.Collections;

public class Word : MonoBehaviour {
    float nextX = 0;
    float nextY = 0;

    public GameObject OneChar;

    public string s = "";

    ArrayList list = new ArrayList();

	void Start () {
        this.init();
	}
	
    virtual public void init()
    {
        foreach(GameObject g in this.list)
        {
            Destroy(g);
        }
        this.list.Clear();

        this.GetComponent<MeshRenderer>().enabled = false;

        this.nextX = transform.position.x;
        this.nextY = transform.position.y;

        for (int i = 0; i < this.s.Length; i++)
        {
            if (this.s[i].ToString() == "\\" && this.s[i + 1].ToString() == "n")
            {
                this.nextX = transform.position.x;
                this.nextY -= this.OneChar.transform.localScale.y;
                i++;
            }
            else
            {
                this.add(this.s[i].ToString());
                this.nextX += this.OneChar.transform.localScale.x;
            }
        }
    }

    protected void add(string ch)
    {
        Vector3 nextPos = new Vector3(this.nextX, this.nextY, transform.position.z - this.OneChar.transform.localScale.z);
        GameObject go = Instantiate(this.OneChar, nextPos, Quaternion.identity) as GameObject;
        go.transform.parent = transform;        
        go.GetComponent<MeshRenderer>().enabled = false;
        list.Add(go);
        OneChar oc = go.GetComponent<OneChar>();        
        oc.ch = ch;
    }
}
