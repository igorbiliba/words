﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {
    GameObject cube = null;
    CharSelected cs;

    bool isCtach = false;

    public GameObject player;
    PlayerMovement playerMovement;
    
    PlayerMovement.RotateState currentState = PlayerMovement.RotateState.left;

    CameraInputGui inp;

    const float force = 130f;
    public const float forceMax = 350f;

    public GameObject CubePos;

    public void updateState(PlayerMovement.RotateState state)
    {
        this.currentState = state;
    }

    bool animHand = false;

    public GameObject handLeft;
    public GameObject handRight;

    public GameObject handChecked;

    float defaultY = 0;
    float checkedY = 0;

    const float speedAnim = 3.5f;

    Vector3 posLeftHand;
    Vector3 posRightHand;

    void Update()
    {
        if (this.inp.buttonForceTouch)
        {
            this.CatchTrow();
        }

        if (this.inp.buttonForce2Touch)
        {
            this.CatchTrow(forceMax);
        }

        if(this.animHand)
        {
            this.posLeftHand = this.handLeft.transform.localPosition;
            this.posRightHand = this.handRight.transform.localPosition;

            if (this.isCtach)
            {
                if(this.posLeftHand.y >= this.checkedY)
                {
                    this.posLeftHand.y = this.checkedY;
                    this.posRightHand.y = this.checkedY;
                    this.animHand = false;
                }
                else
                {
                    this.posLeftHand.y += Time.deltaTime * speedAnim;
                    this.posRightHand.y += Time.deltaTime * speedAnim;
                }
            }
            else
            {
                if (this.posLeftHand.y <= this.defaultY)
                {
                    this.posLeftHand.y = this.defaultY;
                    this.posRightHand.y = this.defaultY;
                    this.animHand = false;
                }
                else
                {
                    this.posLeftHand.y -= Time.deltaTime * speedAnim;
                    this.posRightHand.y -= Time.deltaTime * speedAnim;
                }
            }

            this.handLeft.transform.localPosition = this.posLeftHand;
            this.handRight.transform.localPosition = this.posRightHand;

        }
    }

    void Start()
    {
        this.playerMovement = this.player.GetComponent<PlayerMovement>();
        this.inp = Transform.FindObjectOfType<Camera>().GetComponent<CameraInputGui>();

        this.defaultY = this.handLeft.transform.localPosition.y;
        this.checkedY = this.handChecked.transform.localPosition.y;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == OneChar.tagName)
        {
            if(this.cube == null)
            {
                this.cube = other.gameObject;

                this.cs = this.cube.AddComponent<CharSelected>();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(!this.isCtach && other.gameObject == this.cube) {
            if (this.cs != null) this.cs.Rm();

            this.cube = null;
            this.cs = null;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == OneChar.tagName)
        {
            if (this.cube == null)
            {
                this.cube = other.gameObject;

                this.cs = this.cube.AddComponent<CharSelected>();
            }
        }
    }

    void checkedTrapAnim()
    {
        this.animHand = true;
    }



    public void CatchTrow(float f = force)
    {
        if (!this.playerMovement.isRotare)
        {
            if (this.isCtach)
            {
                if(this.cube != null)
                {
                    Rigidbody rb = this.cube.GetComponent<Rigidbody>();
                    rb.isKinematic = false;

                    this.cube.transform.parent = null;

                    rb.AddForce(transform.forward * f);
                }
                
                this.isCtach = false;
                this.checkedTrapAnim();

                if (this.cs != null) this.cs.Rm();
                this.cube = null;
                this.cs = null;
            }
            else
            {
                if(this.cube != null)
                {
                    Rigidbody rb = this.cube.GetComponent<Rigidbody>();
                    rb.isKinematic = true;

                    this.cube.transform.parent = this.CubePos.transform;
                    this.cube.transform.localPosition = new Vector3(0, 0, 0);

                    this.isCtach = true;

                    this.checkedTrapAnim();
                }
            }
        }
    }
}
