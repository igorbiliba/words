﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    const string ungroudet = "ungroudet";

    Camera cam;
    CameraInputGui camGui;
    Rigidbody playerRigitbody;

    float jumpVelocity = 5.5f;
    float speed = 3f;
    Vector3 movement;

    public GameObject body;

    public GameObject trap;
    Trap tr;


    float val = 0;
    float rotY = -90f;
    const int TIME_ROTATE = 700;
    float rotateAnlge = 0;
    public bool isRotare = false;
    public enum RotateState
    {
        left, right
    }

    public GameObject leftBorder;
    public GameObject rightBorder;

    const float marginBorder = 0.5f;

    float minLeft = -1000;
    float maxLeft = 1000;

    void Start () {
        this.cam = Transform.FindObjectOfType<Camera>() as Camera;        
        this.camGui = this.cam.GetComponent<CameraInputGui>();
        this.playerRigitbody = GetComponent<Rigidbody>();
        this.tr = this.trap.GetComponent<Trap>();

        this.minLeft = this.leftBorder.transform.position.x + marginBorder;
        this.maxLeft = this.rightBorder.transform.position.x - marginBorder;
    }

    Vector3 playerTempPos;
    void Update () {
        if(transform.position.x < this.minLeft)
        {
            this.playerTempPos = transform.position;
            this.playerTempPos.x = this.minLeft;
            transform.position = this.playerTempPos;
        }
        else if (transform.position.x > this.maxLeft)
        {
            this.playerTempPos = transform.position;
            this.playerTempPos.x = this.maxLeft;
            transform.position = this.playerTempPos;
        }

        this.move();

        if(this.camGui.buttonLeftTouch)
        {
            this.rotate(RotateState.left);
        }
        else if (this.camGui.buttonRightTouch)
        {
            this.rotate(RotateState.right);
        }

        if (this.camGui.buttonUpTouch)
        {
            if(this.isGroud()) this.jump();
        }
        
        if (this.isRotare)
        {
            if (this.rotateAnlge > 0)
            {
                this.val = Time.deltaTime * TIME_ROTATE;
                this.rotY += this.val;
                if (this.rotY >= this.rotateAnlge)
                {
                    this.isRotare = false;
                    this.tr.updateState(PlayerMovement.RotateState.left);

                    this.val += this.rotateAnlge - this.rotY;
                    this.rotY = 90f;
                }
            }
            else
            {
                this.val = Time.deltaTime * TIME_ROTATE * -1;
                this.rotY += this.val;
                if (this.rotY <= this.rotateAnlge)
                {
                    this.isRotare = false;
                    this.tr.updateState(PlayerMovement.RotateState.right);

                    this.val += this.rotateAnlge - this.rotY;
                    this.rotY = -90f;
                }
            }

            this.body.transform.Rotate(new Vector3(0, val, 0));
        }
    }

    void rotate(RotateState rs)
    {
        if (rs == RotateState.left)
        {
            rotateAnlge = -90f;
        }
        else
        {
            rotateAnlge = 90f;
        }
        isRotare = true;
    }

    void jump()
    {
        Vector3 vc = playerRigitbody.velocity;
        vc.y = this.jumpVelocity;
        playerRigitbody.velocity = vc;
    }

    bool isGroud()
    {
        RaycastHit hit;

        float dist = (transform.localScale.y - 0.0000001f) / 2f;        
        if(Physics.SphereCast(transform.position, transform.localScale.x/2, Vector3.down, out hit, dist))
        {
            if (hit.transform.tag != ungroudet) return true;
        }

        return false;
    }

    int dorectionHoriz = 0;
    void move()
    {
        this.dorectionHoriz = 0;

        if(this.camGui.buttonLeftTouch)
        {
            this.dorectionHoriz = -1;
        }
        else if(this.camGui.buttonRightTouch)
        {
            this.dorectionHoriz = 1;
        }

        if(this.dorectionHoriz != 0)
        {
            movement.Set(this.dorectionHoriz, 0f, 0);
            movement = movement.normalized * speed * Time.deltaTime;
            this.playerRigitbody.MovePosition(transform.position + movement);
        }
    }
}
