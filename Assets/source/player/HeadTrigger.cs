﻿using UnityEngine;
using System.Collections;

public class HeadTrigger : MonoBehaviour {

    const float standartPanchCube = 1f;

	void OnTriggerExit(Collider other)
    {
        if(other.tag == OneChar.tagName)
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();
            rb.AddForce(Vector3.up * standartPanchCube);
        }
    }
}
