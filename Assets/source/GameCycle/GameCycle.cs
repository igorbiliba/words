﻿using UnityEngine;
using System.Collections;

public class GameCycle : MonoBehaviour {

    const float timeForOneChar = 5f;
    public GameObject timerWord;
    TimerWord timer;

    public GameObject wordLine;
    WordLine line;

    public GameObject charGenerator;
    CharGenerator generator;

    const int standardLenWord = 3;
    int lenWord = standardLenWord;
    const int maxLenWord = 8;

    int iterationWord = 0;
    const int maxIterationWord = 5;

    BaseData data;

    public GameObject lightObj;
    Flash fl;
    

    public GameObject wordObj;
    Word word;

    public TextAsset file3;
    public TextAsset file4;
    public TextAsset file5;
    public TextAsset file6;
    public TextAsset file7;
    public TextAsset file8;

    void Start () {
        data = new BaseData();
        
        if( !data.LoadInResoures(3)) data.addFile(this.file3, 3);
        if (!data.LoadInResoures(4)) data.addFile(this.file4, 4);
        if (!data.LoadInResoures(5)) data.addFile(this.file5, 5);
        if (!data.LoadInResoures(6)) data.addFile(this.file6, 6);
        if (!data.LoadInResoures(7)) data.addFile(this.file7, 7);
        if (!data.LoadInResoures(8)) data.addFile(this.file8, 8);

        this.timer = this.timerWord.GetComponent<TimerWord>();
        this.generator = this.charGenerator.GetComponent<CharGenerator>();
        this.line = this.wordLine.GetComponent<WordLine>();
        this.word = this.wordObj.GetComponent<Word>();
        this.fl = this.lightObj.GetComponent<Flash>();

        this.generatePartTwo();
    }

    void moveLenWord()
    {
        if(this.iterationWord++ > maxIterationWord)
        {
            this.iterationWord = 0;

            this.lenWord++;

            if(this.lenWord > maxLenWord)
            {
                this.lenWord = maxLenWord;
            }
        }
    }

    bool isFirstStart = true;

    void generate()
    {
        if (!this.isFirstStart)
        {
            float addTime = this.lenWord * timeForOneChar;
            addTime += addTime / 2;            
            this.timer.addI(addTime);            
            this.fl.nextLevel();
        }
    }

    public void generatePartTwo()
    {
        this.moveLenWord();

        this.word.s = "Сложность: " + (this.lenWord - standardLenWord).ToString();
        
        this.word.init();
        
        CurrentWord cv = this.data.getFree(this.lenWord);
        
        while (!cv.isOk())
        {
            cv = this.data.getFree(this.lenWord);
        }
        
        this.generator.generate(cv);
        this.line.setCurrentWord(cv);
        
        this.isFirstStart = false;

        SaveScore.saveLenMax(this.lenWord);
    }
    
    public void complete()
    {
        this.generate();
    }
    
    public void timerOut()
    {
        this.timerWord.GetComponent<timeoutTimer>().enabled = true;
        Camera cam = Transform.FindObjectOfType<Camera>();
        CameraInputGui cig = cam.GetComponent<CameraInputGui>();
        cig.hideButton = true;
        cig.startAnimButtons = true;
    }
}
