﻿using UnityEngine;
using System.Collections;

public class timeoutTimer : MonoBehaviour {
    
    public GameObject goTimerPos;

    const float step = 1000f;
    
    // Use this for initialization
	void Start () {
        
    }


    const float blinkTimeDefault = 0.4f;
    float blinkTime = 2f;

    const int COUNT_BLINK_FOR_RELOAD_LEVEL = 4;
    int countBlink = 0;

	// Update is called once per frame
	void Update () {
        transform.position += (goTimerPos.transform.position - transform.position) / (step * Time.deltaTime);


        if(this.blinkTime > 0)
        {
            this.blinkTime -= Time.deltaTime;
        }
        else
        {
            if(this.countBlink++ > COUNT_BLINK_FOR_RELOAD_LEVEL)
            {
                this.goTimerPos.transform.parent.gameObject.AddComponent<EndLevelEffect>();
                Destroy(this);
            }

            this.blinkTime = blinkTimeDefault;

            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(!child.gameObject.active);
            }
        }
    }
}
