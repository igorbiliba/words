﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public class BaseData {
    struct DataStruct
    {
        public int len;
        public TextAsset ta;
        List<String> list;

        /*
        серилизует json в массив
        */
        public void InitTextAsset()
        {
            this.list = new List<string>();

            string json = ta.text;
            string[] all = this.explode(":", json);
            this.list.AddRange(all);
        }
        
        private string[] explode(string separator, string source)
        {
            return source.Split(new string[] { separator }, StringSplitOptions.None);
        }

        public CurrentWord GetWordCombinations()
        {
            CurrentWord cv = new CurrentWord();
            
            System.Random r = new System.Random();
            int currentIndex = r.Next(0, this.list.Count - 1);
            string currentWord = this.list[currentIndex];

            if(currentWord != "")
            {
                cv.addWord(currentWord);
                this.list.RemoveAt(currentIndex);

                cv.FindSiblings(ref this.list);
            }

            return cv;
        }
    }

    ArrayList list = new ArrayList();

    public CurrentWord getFree(int len)
    {
        foreach(DataStruct i in this.list)
        {
            if(i.len == len)
            {
                return i.GetWordCombinations();
            }
        }

        return new CurrentWord();
    }

    /*
        Проверяет есть лм такие данные в ресурсах, если есть, то загружает
    */
    public bool LoadInResoures(int len)
    {
        return false;
    }

    public void addFile(TextAsset ta, int len)
    {
        DataStruct ds = new DataStruct();
        ds.len = len;
        ds.ta = ta;
        ds.InitTextAsset();
        this.list.Add(ds);
    }

}
