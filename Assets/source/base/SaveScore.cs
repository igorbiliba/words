﻿using UnityEngine;
using System.Collections;

public class SaveScore {
    const string LenMaxKey = "LenMax";

    public static int getLenMax()
    {
        if(PlayerPrefs.HasKey(LenMaxKey))
        {
            return PlayerPrefs.GetInt(LenMaxKey);
        }

        return 0;
    }

    public static void saveLenMax(int i)
    {
        if(getLenMax() < i)
            PlayerPrefs.SetInt(LenMaxKey, i);
    }
}
