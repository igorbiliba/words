﻿using UnityEngine;
using System.Collections;

public class Flash : MonoBehaviour {
    bool isAnim;
    bool flashAmin = true;
    Light l;

    const float intensive = 0.5f;

    public GameObject gcObj;
    GameCycle gc;

    void Start()
    {
        this.l = GetComponent<Light>();

        if(this.gcObj != null) this.gc = this.gcObj.GetComponent<GameCycle>();

    }
	
	void FixedUpdate () {
	
        if(this.isAnim)
        {
            if(this.flashAmin)
            {
                this.l.intensity += intensive;

                if(this.l.intensity >= 8)
                {
                    this.l.intensity = 8;
                    this.isAnim = false;

                    this.nextLevel(false);
                }
            }
            else
            {
                this.l.intensity -= intensive;

                if (this.l.intensity <= 1)
                {
                    this.l.intensity = 1;
                    this.isAnim = false;
                }
            }
        }
	}

    public void nextLevel(bool direct = true)
    {
        this.isAnim = true;
        this.flashAmin = direct;

        if(!direct)
        {
            if (this.gcObj != null) this.gc.generatePartTwo();
        }
    }
}
