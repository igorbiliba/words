﻿using UnityEngine;
using System.Collections;

public class levetationObj : MonoBehaviour {

    const float scaleMin = 12f;
    const float scaleMax = 15f;

    Rigidbody currentRigitBody;

    const float rotationMin = 0;
    const float rotationMax = 111;

    const float forceRotationMin = 5;
    const float forceRotationMax = 30;

    const float forceMin = 70;
    const float forceMax = 200;

    public const float xPosMax = 80f;
    public const float xPosMin = -80f;

    public const float rotateMax = 1f;
    public const float rotateMin = -0f;

    levetationEngine le;

    const float marginExit = 20;

    float maxLeftX = 0;
    float maxRightX = 0;

    public void setLevevationIngine(levetationEngine _le)
    {
        this.maxLeftX = xPosMin - marginExit;
        this.maxRightX = xPosMax + marginExit;

        this.le = _le;
    }
    
	void FixedUpdate () {

	    if(this.transform.position.x > (this.maxRightX) || this.transform.position.x < (this.maxLeftX))
        {
            this.le.create();
            Destroy(gameObject);
        }
	}

    public void randomSize()
    {
        Vector3 scaleVal = this.transform.localScale;
        scaleVal.x = Random.Range(scaleMin, scaleMax);
        scaleVal.y = Random.Range(scaleMin, scaleMax);
        scaleVal.z = Random.Range(scaleMin, scaleMax);
        this.transform.localScale = scaleVal;
    }

    public void randomRotarion()
    {
        Vector3 rotVal = this.transform.localScale;
        rotVal.x = Random.Range(rotateMin, rotateMax);
        rotVal.y = Random.Range(rotateMin, rotateMax);
        rotVal.z = Random.Range(rotateMin, rotateMax);

        this.currentRigitBody = GetComponent<Rigidbody>();
        float forceRotation = Random.RandomRange(forceRotationMin, forceRotationMax);
        this.currentRigitBody.AddTorque(rotVal * forceRotation);
    }

    public void addRandomForce(float direction)
    {
        this.currentRigitBody = GetComponent<Rigidbody>();
        float forceMove = Random.RandomRange(forceMin, forceMax);

        if(direction > 0) this.currentRigitBody.AddForce(Vector3.left * forceMove);
        else this.currentRigitBody.AddForce(Vector3.right * forceMove);
    }
}
