﻿using UnityEngine;
using System.Collections;

public class levetationEngine : MonoBehaviour {
    public GameObject obj;
    ArrayList levetaionList = new ArrayList();

    const float zMin = 20f;
    const float zMax = 60f;
    
    public float xPos = levetationObj.xPosMax;
    float yPos = 5;

    const float timeVal = 3f;
    float time = timeVal;
    int limit = 4;

    float[] zList;

    void Start() {
        this.zList = new float[this.limit];

        float midlePos = zMax - zMin;
        for (int i = 0; i < this.limit; i++)
        {
            this.zList[i] = ((midlePos / this.limit) * i) + zMin;
        }

        //this.create();
    }

    int currentZKey = 999;
    protected float getNextZ()
    {
        this.currentZKey++;
        if(this.currentZKey >= this.zList.Length)
        {
            this.currentZKey = 0;
        }

        return this.zList[this.currentZKey];
    }

    public void create()
    {
        float z = this.getNextZ();

        this.xPos *= -1;
        this.yPos *= -1;

        Vector3 pos = new Vector3(xPos, yPos, z);
        GameObject insObj = Instantiate(this.obj, pos, Quaternion.identity) as GameObject;

        //добавить в список
        levetationObj lo = insObj.GetComponent<levetationObj>();
        this.levetaionList.Add(lo);
        
        //размеры
        lo.randomSize();

        //добавить скорость
        lo.addRandomForce(this.xPos);

        //добавить вращение
        lo.randomRotarion();

        //вписывам себя для обраинызх вызово в случае ухода со сцены
        lo.setLevevationIngine(this);
    }

	void Update () {
        if(this.limit > 0)
        {
            if (this.time > 0)
            {
                this.time -= Time.deltaTime;
            }
            else
            {
                this.create();
                this.time = timeVal;
                this.limit--;
            }
        }
	}
}
